module Samir.Message
    ( Message
    , keyboardMsg
    , msgBody
    , msgKeyboard
    , noKeyboardMsg
    ) where

import Data.String (IsString(..))
import Data.Text (Text, pack)

data Message = Message
    { _mText :: Text
    , _mKeyboard :: Maybe [Text]
    } deriving (Eq, Show)

instance IsString Message where
    fromString = noKeyboardMsg . pack

noKeyboardMsg :: Text -> Message
noKeyboardMsg txt = Message txt Nothing

keyboardMsg :: Text -> [Text] -> Message
keyboardMsg txt kbd = Message txt (Just kbd)

msgBody :: Message -> Text
msgBody (Message txt _) = txt

msgKeyboard :: Message -> Maybe [Text]
msgKeyboard (Message _ kbd) = kbd
