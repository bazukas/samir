module Samir.Lang.Intent
    ( module Samir.Lang.Intent.Classifier
    , module Samir.Lang.Intent.Types
    ) where

import Samir.Lang.Intent.Classifier
import Samir.Lang.Intent.Types
