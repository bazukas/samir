{-# LANGUAGE DeriveGeneric, GADTs #-}
module Samir.Lang.Intent.Types
    ( Intent(..)
    , IntentName(..)
    , Slot(..)
    , SlotForm
    , SlotFill
    , SlotPartialFill
    , SlotName(..)
    , IntentConfig(..)
    , SlotConfig(..)
    , R(..)
    , ML(..)
    , U
    , RuleType(..)
    , IntentRule(..)
    , MLClass
    , staticAnswer
    , regexRules
    , mlRules
    ) where

import Control.DeepSeq (NFData)
import Data.Binary (Binary)
import Data.String (IsString(..))
import Data.Text (Text, pack)
import GHC.Generics (Generic)
import System.Random (randomRIO)
import qualified Data.Map.Strict as Map

import Samir.Lang.Entity.Types (Entity, EntityType)
import Samir.Message (Message, noKeyboardMsg)

newtype IntentName = IntentName Text
    deriving (Show, Eq, Ord, Generic)
instance Binary IntentName
instance NFData IntentName
instance IsString IntentName where
    fromString = IntentName . pack

newtype SlotName = SlotName Text
    deriving (Show, Eq, Ord)
instance IsString SlotName where
    fromString = SlotName . pack

-- TODO: static guarantees
type SlotForm = Map.Map SlotName Slot
type SlotFill = Map.Map SlotName Entity
type SlotPartialFill = Map.Map SlotName Entity

newtype MLClass = MLClass Text
    deriving (Show, Eq, Ord, Generic)
instance Binary MLClass
instance NFData MLClass
instance IsString MLClass where
    fromString = MLClass . pack

data R = R String [(Int, EntityType)]
data ML = ML MLClass [Text]
data U
data RuleType a where
    IntentRegexRule :: R -> RuleType R
    IntentMLRule :: ML -> RuleType ML
    IntentPresetMLRule :: MLClass -> RuleType ML
    IntentRegexRule' :: R -> RuleType U
    IntentMLRule' :: ML -> RuleType U
    IntentPresetMLRule' :: MLClass -> RuleType U
data IntentRule a = IntentRule (RuleType a) [SlotName]

type SamirAction = SlotFill -> IO Message

data Intent = Intent
    { iName :: IntentName
    , iSlots :: SlotForm
    , iAction :: SamirAction
    , iFillSlots :: Text -> IO (Maybe SlotPartialFill)
    , iFallback :: Maybe Message
    }

data Slot = Slot
    { sName :: SlotName
    , sType :: EntityType
    , sRequest :: Message
    , sMatch :: Text -> IO (Maybe Entity)
    , sFallback :: Maybe Message
    }

data SlotConfig = SlotConfig
    { scName :: SlotName
    , scType :: EntityType
    , scRequest :: Message
    , scResponseParser :: [IntentRule R]
    , scFallback :: Maybe Message
    }

data IntentConfig = IntentConfig
    { icName :: IntentName
    , icSlots :: [SlotConfig]
    , icActivateRules :: [IntentRule U]
    , icSlotFillRules :: [IntentRule R]
    , icFallback :: Maybe Message
    , icAction :: SamirAction
    }

staticAnswer :: [Text] -> SamirAction
staticAnswer xs _ = randomRIO (0, length xs - 1) >>= fmap (return . noKeyboardMsg) (xs !!)

regexRules :: [IntentRule U] -> [IntentRule R]
regexRules = foldr go []
    where go :: IntentRule U -> [IntentRule R] -> [IntentRule R]
          go (IntentRule (IntentRegexRule' r) ns) b = IntentRule (IntentRegexRule r) ns:b
          go _ b = b

mlRules :: [IntentRule U] -> [IntentRule ML]
mlRules = foldr go []
    where go :: IntentRule U -> [IntentRule ML] -> [IntentRule ML]
          go (IntentRule (IntentMLRule' ml) ns) b = IntentRule (IntentMLRule ml) ns:b
          go (IntentRule (IntentPresetMLRule' cl) ns) b = IntentRule (IntentPresetMLRule cl) ns:b
          go _ b = b
