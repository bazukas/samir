{-# LANGUAGE GADTs, LambdaCase #-}
module Samir.Lang.Intent.Classifier
    ( IntentClassifier(..)
    , MLIntentClassifier(..)
    , PresetMLData(..)
    , MLTrainParams(..)
    , MLParams(..)
    , buildIntentClassifier
    , buildMLClassifier
    , matchIntent
    ) where

import AI.SVM.Simple (SVMClassifier, Kernel(..), ClassifierType(..), classifyProbability, trainClassifier)
import Control.DeepSeq (force)
import Control.Exception (evaluate)
import Control.Monad.Trans.Maybe (MaybeT(..), runMaybeT)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Text (Text, pack, unpack)
import Data.Time.LocalTime (TimeZone)
import Text.Regex.TDFA
import qualified Data.Array as Array
import qualified Data.Map.Strict as Map

import Samir.Lang.Entity.Extract (extractEntities)
import Samir.Lang.Entity.Types (Entity(..), EntityType(..))
import Samir.Lang.Intent.Types
import Samir.Lang.Preprocess (vectorizeText)
import Samir.Lang.Vectorizer (Vectorizer)

data IntentClassifier = IntentClassifier
    { _icRegexRules :: [(IntentRule R, Intent)]
    , _icMLClassifier :: MLIntentClassifier
    , _icMLThreshold :: Double
    , _icTimeZone :: TimeZone
    }

-- first, check all regex rules
-- then, check ml rules
matchIntent :: IntentClassifier -> Text -> IO (Maybe (Intent, SlotPartialFill))
matchIntent (IntentClassifier rs ml thresh tz) txt = go rs
    where go ((r,i):xs) = regexMatch r txt tz >>= \case
                              Nothing -> go xs
                              Just pf -> return $ Just (i, pf)
          go [] = runMaybeT $ do
              (i, slots, cf) <- MaybeT $ return $ mlCheckIntent ml txt
              if cf >= thresh then do
                  pf <- MaybeT $ Just <$> extractEntities tz txt (slotTypes i slots)
                  return (i, Map.fromList . catMaybes . fmap sequence $ zip slots pf)
              else MaybeT $ return Nothing
          slotTypes i slots = catMaybes $ fmap (\s -> sType <$> Map.lookup s (iSlots i)) slots --TODO: catMaybes


buildIntentClassifier :: [IntentConfig] -> MLIntentClassifier -> Double -> TimeZone -> IntentClassifier
buildIntentClassifier is ml th tz = IntentClassifier rules ml th tz
    where rules = concatMap (\(ic, i) -> fmap (\r -> (r, i)) (regexRules . icActivateRules $ ic)) zipped
          zipped = zip is (fmap (config2intent tz) is)

config2slot :: TimeZone -> SlotConfig -> Slot
config2slot tz (SlotConfig sn st req rules fb) = Slot sn st req (matchResp rules) fb
    where matchResp [] _ = return Nothing
          matchResp (r:rs) txt =
              regexMatch r txt tz >>= \case
                Nothing -> matchResp rs txt
                Just pf -> case Map.lookup sn pf of
                                  Nothing -> matchResp rs txt
                                  Just e -> return $ Just e

config2intent :: TimeZone -> IntentConfig -> Intent
config2intent tz (IntentConfig n slots _ sfrs fb ac) = Intent n sf ac (fillSlots sfrs) fb
    where sf = Map.fromList . fmap (\sc -> (scName sc, config2slot tz sc)) $ slots
          fillSlots [] _ = return Nothing
          fillSlots (r:rs) txt =
              regexMatch r txt tz >>= \case
                Nothing -> fillSlots rs txt
                Just pf -> return $ Just pf

-- Machine Learning

data MLIntentClassifier = MLIntentClassifier
    { _mlSvm :: SVMClassifier MLClass
    , _mlIntentMap :: Map.Map MLClass (Intent, [SlotName])
    , _mlVectorizer :: Vectorizer
    }
type TrainData = [(MLClass, [Text])]
newtype PresetMLData = PresetMLData (Map.Map MLClass [Text])
data MLTrainParams = MLTrainParams
    { _mltpTrainC :: Maybe Double
    , _mltpTrainGamma :: Maybe Double
    }
data MLParams = MLParams
    { _mlpPreset :: PresetMLData
    , _mlpTrainParams :: MLTrainParams
    , _mlpVectorizer :: Vectorizer
    }

defaultC :: Double
defaultC = 10

defaultGamma :: Double
defaultGamma = 1

trainMLClassifier :: MLTrainParams -> TrainData -> Vectorizer -> IO (SVMClassifier MLClass)
trainMLClassifier (MLTrainParams mC mG) td v = evaluate $ force $ snd $ trainClassifier classifierType kernelType trainingData'
    where trainingData = (fmap . fmap) (vectorizeText v) (flattenData td)
          trainingData' = catMaybes $ fmap sequence trainingData
          flattenData = concatMap sequence
          classifierType = C (fromMaybe defaultC mC)
          kernelType = RBF (fromMaybe defaultGamma mG)

buildMLClassifier :: MLParams -> TimeZone -> [IntentConfig] -> IO MLIntentClassifier
buildMLClassifier (MLParams (PresetMLData preset) p v) tz is = do
    svm <- trainMLClassifier p (Map.toList td) v
    return $ MLIntentClassifier svm intentMap v
    where intentMap = Map.fromList $ fmap getMapping mlRules'
          td = Map.union (Map.fromList (catMaybes (fmap getTd mlRules'))) preset
          mlRules' :: [(Intent, IntentRule ML)]
          mlRules' = concatMap (\ic -> sequence (config2intent tz ic, mlRules (icActivateRules ic))) is
          getTd :: (Intent, IntentRule ML) -> Maybe (MLClass, [Text])
          getTd (_, IntentRule (IntentMLRule (ML cl txts)) _) = Just (cl, txts)
          getTd _ = Nothing
          getMapping :: (Intent, IntentRule ML) -> (MLClass, (Intent, [SlotName]))
          getMapping (i, IntentRule (IntentMLRule (ML cl _)) ns) = (cl, (i, ns))
          getMapping (i, IntentRule (IntentPresetMLRule cl) ns) = (cl, (i, ns))

mlCheckIntent :: MLIntentClassifier -> Text -> Maybe (Intent, [SlotName], Double)
mlCheckIntent (MLIntentClassifier svm im v) txt = do
    vectorized <- vectorizeText v txt
    (cl, p) <- Just $ classifyProbability svm vectorized
    (i, slots) <- Map.lookup cl im
    return (i, slots, p)

-- REGEX

fill :: [(Int, EntityType)] -> [String] -> Maybe [(Text, EntityType)]
fill [] _ = Just []
fill ns (_:caps)
  | maxIndex ns > length caps = Nothing
  | otherwise = Just $ fmap toEntity ns
  where toEntity :: (Int, EntityType) -> (Text, EntityType)
        toEntity (ind, entType) = (pack $ caps !! (ind-1), entType)
        maxIndex xs = maximum $ fmap fst xs
fill _ _ = Nothing

-- TODO: possibly slow
regexMatch :: IntentRule R -> Text -> TimeZone -> IO (Maybe SlotPartialFill)
regexMatch (IntentRule (IntentRegexRule (R r slots)) ns) txt tz
  | length slots /= length ns = return Nothing
  | otherwise = case matchOnceText (regex r) (unpack txt) of
                  Nothing -> return Nothing
                  Just (_, arr, _) ->
                      case fill slots $ fst <$> Array.elems arr of
                        Nothing -> return Nothing
                        Just entTuples -> sequence . Map.fromList . zip ns <$> extractEnts entTuples
  where extractEnts :: [(Text, EntityType)] -> IO [Maybe Entity]
        extractEnts = traverse extractEnt
        extractEnt (t, RawE) = return . Just . Raw $ t
        extractEnt (t, tp) = head <$> extractEntities tz t [tp]

regex :: String -> Regex
regex = makeRegexOpts compOpts execOpts
    where compOpts = defaultCompOpt { caseSensitive = False }
          execOpts = defaultExecOpt
