{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
module Samir.Lang.Vectorizer
    ( Vectorizer
    , TextVector
    , avgTextVector
    , loadVectorizer
    , wordVector
    ) where

import Control.DeepSeq (NFData, force)
import Control.Exception (evaluate)
import Data.List (genericLength, foldl')
import Data.Maybe (catMaybes)
import GHC.Generics (Generic)
import Numeric.LinearAlgebra (Vector, fromList)
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Read as T

type TextVector = Vector Double

newtype Vectorizer = Vectorizer (Map.Map T.Text TextVector)
    deriving (Generic, NFData, Show)

parseDouble :: T.Text -> Maybe Double
parseDouble txt = case T.double txt of
                    Left _ -> Nothing
                    Right (d, _) -> Just d

parseLine :: T.Text -> Maybe (T.Text, TextVector)
parseLine line
    | [] <- lwords = Nothing
    | (k:v) <- lwords = case traverse parseDouble v of
                          Nothing -> Nothing
                          Just vec -> Just (k, fromList vec)
    where lwords = T.words line

loadVectorizer :: FilePath -> IO Vectorizer
loadVectorizer f = do
    dat <- fmap T.lines (T.readFile f)
    let vecList = catMaybes $ fmap parseLine dat
    evaluate $ force  $ Vectorizer (Map.fromList vecList)

wordVector :: Vectorizer -> T.Text -> Maybe TextVector
wordVector (Vectorizer v) w = Map.lookup w v

avgTextVector :: Vectorizer -> [T.Text] -> Maybe TextVector
avgTextVector v txts = case catMaybes $ fmap (wordVector v) txts of
                          [] -> Nothing
                          x:xs -> Just $ foldl' (+) x xs / (genericLength xs + 1)
