{-# LANGUAGE GADTs #-}
module Samir.Lang.Entity.Extract
    ( extractEntities
    ) where

import Data.List (partition)
import Data.Maybe (catMaybes)
import Data.Text (Text)
import Data.Time.Clock (getCurrentTime)
import Data.Time.LocalTime (TimeZone, utcToZonedTime)
import qualified Duckling.Core as D
import qualified Duckling.Numeral.Types as D
import qualified Duckling.Ordinal.Types as D
import qualified Duckling.PhoneNumber.Types as D
import qualified Duckling.Url.Types as D
import qualified Duckling.Time.Types as D

import Samir.Lang.Entity.Types

ducklingDimension :: EntityType -> Maybe (D.Some D.Dimension)
ducklingDimension NumeralE = Just $ D.This D.Numeral
ducklingDimension OrdinalE = Just $ D.This D.Ordinal
ducklingDimension PhoneNumberE = Just $ D.This D.PhoneNumber
ducklingDimension URLE = Just $ D.This D.Url
ducklingDimension TimeE = Just $ D.This D.Time
ducklingDimension _ = Nothing

duckling2samir :: D.Entity -> Maybe Entity
duckling2samir (D.Entity dim _ rval _ _ _ _)
  | dim == "number", (D.RVal D.Numeral (D.NumeralValue val)) <- rval = Just $ Numeral val
  | dim == "ordinal", (D.RVal D.Ordinal (D.OrdinalData val)) <- rval = Just $ Ordinal val
  | dim == "phone-number", (D.RVal D.PhoneNumber (D.PhoneNumberValue val)) <- rval = Just $ PhoneNumber val
  | dim == "url", (D.RVal D.Url (D.UrlData val _)) <- rval = Just $ URL val
  | dim == "time", (D.RVal D.Time (D.TimeValue (D.SimpleValue (D.InstantValue val _)) _ _)) <- rval =
      Just $ Time val
duckling2samir _ = Nothing

makeContext :: TimeZone -> IO D.Context
makeContext tz = D.Context <$> time <*> pure locale
    where locale = D.makeLocale D.RU Nothing
          time = getCurrentTime >>= \t -> return (D.fromZonedTime . utcToZonedTime tz $ t)

options :: Bool -> D.Options
options = D.Options

align :: [EntityType] -> [Entity] -> [Maybe Entity]
align entTypes ents = fst $ foldr go ([], ents) entTypes
    where go t (m, e) =
            case partition ((==t) . entityType) e of
              ([],_) -> (Nothing:m, e)
              (x:xs,ys) -> (Just x:m, xs++ys) -- TODO: maybe not efficient

extractEntities' :: D.Context -> Text -> [EntityType] -> [Maybe Entity]
extractEntities' ctx txt entTypes =
    case traverse ducklingDimension entTypes of
      Nothing -> fmap (const Nothing) entTypes -- TODO: fail better or typecheck
      Just duckEntTypes -> align entTypes ents
        where ents = catMaybes $ fmap duckling2samir duckEnts
              duckEnts = D.parse txt ctx (options False) duckEntTypes

-- TODO: static check for same length
extractEntities :: TimeZone -> Text -> [EntityType] -> IO [Maybe Entity]
extractEntities tz txt ents = do
    ctx <- makeContext tz
    return $ extractEntities' ctx txt ents
