{-# LANGUAGE LambdaCase #-}
module Samir.Lang.Entity.Types
    ( Entity(..)
    , EntityType(..)
    , entityType
    , formatEntity
    , formatTimeEntity
    ) where

import Data.Aeson.Types (typeMismatch)
import Data.Text (Text, pack)
import Data.Time.LocalTime (ZonedTime, zonedTimeToUTC)
import Data.Yaml (FromJSON(..), withText)
import qualified Data.Time.Format as TF

data EntityType
  = NumeralE
  | OrdinalE
  | PhoneNumberE
  | URLE
  | TimeE
  | RawE
  deriving (Eq, Ord, Show)

instance FromJSON EntityType where
    parseJSON v = withText "EntityType" (\case
          "num" -> return NumeralE
          "ord" -> return OrdinalE
          "phone" -> return PhoneNumberE
          "url" -> return URLE
          "time" -> return TimeE
          "raw" -> return RawE
          _ -> typeMismatch "EntityType" v) v

data Entity
  = Numeral Double
  | Ordinal Int
  | PhoneNumber Text
  | URL Text
  | Time ZonedTime
  | Raw Text
  deriving (Show)

instance Eq Entity where
    (==) (Numeral e1) (Numeral e2) = e1 == e2
    (==) (Ordinal e1) (Ordinal e2) = e1 == e2
    (==) (PhoneNumber e1) (PhoneNumber e2) = e1 == e2
    (==) (URL e1) (URL e2) = e1 == e2
    (==) (Raw e1) (Raw e2) = e1 == e2
    (==) (Time e1) (Time e2) = zonedTimeToUTC e1 == zonedTimeToUTC e2
    (==) _ _ = False

entityType :: Entity -> EntityType
entityType (Numeral _) = NumeralE
entityType (Ordinal _) = OrdinalE
entityType (PhoneNumber _) = PhoneNumberE
entityType (URL _) = URLE
entityType (Time _) = TimeE
entityType (Raw _) = RawE

formatEntity :: Entity -> Text
formatEntity (Numeral d) = pack . show $ d
formatEntity (Ordinal i) = pack . show $ i
formatEntity (PhoneNumber p) = p
formatEntity (URL u) = u
formatEntity (Time t) = pack . show $ t
formatEntity (Raw t) = t

formatTimeEntity :: Entity -> String -> Text
formatTimeEntity (Time t) f = pack $ TF.formatTime TF.defaultTimeLocale f t
formatTimeEntity _ _ = ""
