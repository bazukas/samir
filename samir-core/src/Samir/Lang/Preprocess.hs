module Samir.Lang.Preprocess
    ( preprocessText
    , vectorizeText
    ) where

import Data.Char (isAlphaNum)
import Data.Text (Text, toLower, any)
import NLP.Tokenize.Text (tokenize)

import Samir.Lang.Vectorizer (Vectorizer, TextVector, avgTextVector)

samirFilter :: Text -> Bool
samirFilter = Data.Text.any isAlphaNum

preprocessText :: Text -> [Text]
preprocessText = filter samirFilter . tokenize . toLower

vectorizeText :: Vectorizer -> Text -> Maybe TextVector
vectorizeText v txt = avgTextVector v (preprocessText txt)
