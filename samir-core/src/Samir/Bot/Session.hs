{-# LANGUAGE LambdaCase #-}
module Samir.Bot.Session
    ( SamirSession(..)
    , getResponse
    , newSession
    ) where

import Data.Text (Text)
import qualified Data.Map.Strict as Map

import Samir.Bot
import Samir.Lang
import Samir.Message

data SamirContext = SamirContext
    { _scIntent :: Intent
    , _scFilled :: SlotPartialFill
    , _scLastRequest :: Maybe Slot
    }

data SamirSession = SamirSession
    { _ssBot :: SamirBot
    , _ssContext :: Maybe SamirContext
    , _ssHistory :: [Text]
    }

newSession :: SamirBot -> SamirSession
newSession bot = SamirSession bot Nothing []

checkFill' :: SamirContext -> Either Slot SlotFill
checkFill' (SamirContext i pf _) = Map.foldrWithKey go (Right Map.empty) (iSlots i)
  where go _ _ (Left s) = Left s
        go sn s (Right m) = case Map.lookup sn pf of
                               Nothing -> Left s
                               Just e -> Right (Map.insert sn e m)

fillRequest :: SamirContext -> Slot -> (Message, SamirContext)
fillRequest (SamirContext i pf _) s@(Slot _ _ r _ _) = (r, SamirContext i pf (Just s))

checkFill :: SamirContext -> IO (Message, Maybe SamirContext)
checkFill ctx@(SamirContext i _ _) =
    case checkFill' ctx of
      Left s -> return (Just <$> fillRequest ctx s)
      Right sf -> iAction i sf >>= \t -> return (t, Nothing) --TODO: don't forget context after filling

-- if new intent is the same as current intent
-- then use current context
checkNewContext :: SamirBot -> Maybe SamirContext -> Text -> IO (Message, Maybe SamirContext)
checkNewContext (SamirBot c gfb hfb) mCtx txt = do
    mIntent <- matchIntent c txt
    case mIntent of
      Nothing -> return (fallback, mCtx)
      Just (i, pf) -> checkFill (newCtx mCtx)
          where newCtx (Just (SamirContext i' pf' _))
                  | iName i' == iName i = SamirContext i (Map.union pf pf') Nothing
                newCtx _ = SamirContext i pf Nothing
    where fallback = case mCtx of
                       Nothing -> hfb                                                   -- if no current context -> helper fallback
                       Just (SamirContext _ _ (Just (Slot _ _ _ _ (Just fb)))) -> fb    -- if there is requested slot and it has fb
                       Just (SamirContext (Intent _ _ _ _ (Just fb)) _ _) -> fb         -- if there is context intent and it has fb
                       Just _ -> gfb                                                    -- otherwise generic fallback

checkInsideContext :: SamirBot -> SamirContext -> Text -> IO (Message, Maybe SamirContext)
checkInsideContext bot ctx@(SamirContext i pf _) txt =
    iFillSlots i txt >>= \case
      Nothing -> checkNewContext bot (Just ctx) txt
      Just pf' -> checkFill (SamirContext i (Map.union pf' pf) Nothing)

getResponse' :: SamirBot -> Maybe SamirContext -> Text -> IO (Message, Maybe SamirContext)
getResponse' bot Nothing txt = checkNewContext bot Nothing txt
getResponse' bot (Just ctx@(SamirContext _ _ Nothing)) txt = checkInsideContext bot ctx txt
getResponse' bot (Just ctx@(SamirContext i pf (Just (Slot sn _ _ match _)))) txt =
    match txt >>= \case
      Nothing -> checkInsideContext bot ctx txt
      Just e -> checkFill (SamirContext i (Map.insert sn e pf) Nothing)

getResponse :: SamirSession -> Text -> IO (SamirSession, Message)
getResponse (SamirSession bot mCtx hist) txt = do
    (resp, nCtx) <- getResponse' bot mCtx txt
    return (SamirSession bot nCtx (msgBody resp:txt:hist), resp)
