module Samir.Bot
    ( SamirBot(..)
    , BotConfig(..)
    , buildSamirBot
    , trainAndBuildSamirBot
    ) where

import Data.Time.LocalTime (TimeZone)

import Samir.Lang
import Samir.Message

data SamirBot = SamirBot
    { bClassifier :: IntentClassifier
    , bGenericFallback :: Message
    , bHelpFallback :: Message
    }

data BotConfig = BotConfig
    { bcIntents :: [IntentConfig]
    , bcGenericFallback :: Message
    , bcHelpFallback :: Message
    , bcThreshold :: Double
    , bcTimeZone :: TimeZone
    , bcLang :: SamirLang
    }

buildSamirBot :: MLIntentClassifier -> BotConfig -> SamirBot
buildSamirBot ml (BotConfig is gfb hfb t tz _) = SamirBot (buildIntentClassifier is ml t tz) gfb hfb

trainAndBuildSamirBot :: MLParams -> BotConfig -> IO SamirBot
trainAndBuildSamirBot p c@(BotConfig is _ _ _ tz _) = do
    ml <- buildMLClassifier p tz is
    return $ buildSamirBot ml c
