import Test.Tasty
import Test.Tasty.Hspec

import Test.Data
import Test.Dialog
import Test.Entity
import Test.Preprocess

main :: IO ()
main = tests >>= defaultMain

tests :: IO TestTree
tests = do
    dat <- loadResources
    testGroup "Tests" <$> sequence
      [ testSpec "Preprocess" (preprocessSpec dat)
      , testSpec "Entity" entitySpec
      , testSpec "Dialog" (dialogSpec dat)
      ]
