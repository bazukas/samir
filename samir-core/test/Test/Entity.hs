module Test.Entity
    ( entitySpec
    ) where

import Test.Tasty.Hspec

import Data.Text (Text)
import Data.Time.Calendar (fromGregorian)
import Data.Time.Clock (UTCTime(..), secondsToDiffTime)
import Data.Time.LocalTime (TimeZone, hoursToTimeZone, utcToZonedTime)

import Samir.Lang

mskTz :: TimeZone
mskTz = hoursToTimeZone 3

extractEntities' :: Text -> [EntityType] -> IO [Maybe Entity]
extractEntities' = extractEntities mskTz

entitySpec :: Spec
entitySpec =
    describe "extractEntities" $ do
        it "extracts number" $
            extractEntities' "в этом тексте число 1 появилось" [NumeralE]
                `shouldReturn` [Just $ Numeral 1.0]
        it "extracts ordinal" $
            extractEntities' "4й в этом году" [OrdinalE]
                `shouldReturn` [Just $ Ordinal 4]
        it "extracts phone" $
            extractEntities' "мой телефон: 89171110101" [PhoneNumberE]
                `shouldReturn` [Just $ PhoneNumber "89171110101"]
        it "extracts url" $
            extractEntities' "доступен по адресу: www.ru" [URLE]
                `shouldReturn` [ Just $ URL "www.ru"]
        it "extracts time" $
            extractEntities' "пойду 13.03.2018 в 16:00" [TimeE]
                `shouldReturn` [Just $ Time (utcToZonedTime mskTz (UTCTime (fromGregorian 2018 3 13) (secondsToDiffTime 13*60*60)))]
