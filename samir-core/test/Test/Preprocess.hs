module Test.Preprocess
    ( preprocessSpec
    ) where

import Test.Tasty.Hspec

import Data.Maybe (isJust)

import Samir.Bot
import Samir.Lang

preprocessSpec :: SamirBot -> Spec
preprocessSpec (SamirBot (IntentClassifier _ (MLIntentClassifier _ _ vec) _ _) _ _) = do
    describe "preprocessText" $
        it "tokenizes and lowers" $
            preprocessText "This is a sample Preprocessing test. What happens?" `shouldBe`
            ["this", "is", "a", "sample", "preprocessing", "test", "what", "happens"]
    describe "vectorizeText" $
        it "returns Just" $
            vectorizeText vec "Hello, this should vectorize" `shouldSatisfy` isJust
