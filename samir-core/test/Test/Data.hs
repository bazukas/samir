module Test.Data where

import Data.Maybe (fromJust)
import Data.String (fromString)
import Data.Text (Text)
import Data.Time.LocalTime (hoursToTimeZone)
import Data.Yaml (decodeFileEither, ParseException)
import qualified Data.Map.Strict as Map
import qualified Data.Text as Text

import Samir.Bot
import Samir.Lang
import Samir.Message

loadPreset :: FilePath -> IO PresetMLData
loadPreset fp = fmap (PresetMLData . etom) (decodeFileEither fp)
    where etom :: Either ParseException (Map.Map String [Text]) -> Map.Map MLClass [Text]
          etom (Left _) = Map.fromList []
          etom (Right m) = Map.mapKeys fromString m

mlTrainParams :: MLTrainParams
mlTrainParams = MLTrainParams (Just 100) (Just 1)

buildMLParams :: Vectorizer -> IO MLParams
buildMLParams v = do
    preset <- loadPreset "../data/mlpreset.yaml"
    return $ MLParams preset mlTrainParams v

loadResources :: IO SamirBot
loadResources = do
    putStrLn "\nLoading vectorizer..."
    vectorizer <- loadVectorizer "../data/fasttext.ru.300.vec"
    putStrLn "Training classifier..."
    params <- buildMLParams vectorizer
    trainAndBuildSamirBot params botConfig

botConfig :: BotConfig
botConfig = BotConfig
    { bcIntents = intents
    , bcGenericFallback = "Извините, я не смог понять ваше последнее сообщение"
    , bcHelpFallback = "Help Fallback"
    , bcThreshold = 0.05
    , bcTimeZone = hoursToTimeZone 3
    , bcLang = RU
    }

intents :: [IntentConfig]
intents = [ IntentConfig "greeting" []
                         [ IntentRule (IntentRegexRule' (R "(привет|здравствуйте)!?" [])) []
                         , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.hello") []
                         , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodmoring") []
                         , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodeveing") []
                         , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodnight") []
                         ]
                         []
                         Nothing
                         (staticAnswer ["Здравствуйте!"])
          , IntentConfig "goodbye" []
                         [ IntentRule (IntentRegexRule' (R "пока!?" [])) []
                         , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.bye") []
                         ]
                         []
                         Nothing
                         (staticAnswer ["До свидания!"])
          , IntentConfig "brb" []
                         [ IntentRule (IntentPresetMLRule' "samir.smalltalk.user.will_be_back") []
                         ]
                         []
                         Nothing
                         (staticAnswer ["Буду ждать!"])
          , IntentConfig "name"
                         [ SlotConfig "name" RawE "Как вас зовут?" [] Nothing ]
                         [ IntentRule (IntentRegexRule' (R "Меня зовут ([А-Яа-я]+)" [(1, RawE)])) ["name"] ]
                         []
                         Nothing
                         (\m -> return (noKeyboardMsg $ Text.concat ["Приятно познакомиться, ", formatEntity . fromJust $ Map.lookup "name" m, "!"]))
          , IntentConfig "url"
                         [ SlotConfig "url" RawE "Какой ваш сайт?" [] Nothing ]
                         [ IntentRule (IntentRegexRule' (R "У меня есть сайт ([a-zA-Z\\-\\.0-9]+)" [(1, URLE)])) ["url"] ]
                         []
                         Nothing
                         (\m -> return (noKeyboardMsg $ Text.concat ["Крутой сайт ", formatEntity . fromJust $ Map.lookup "url" m, "!"]))
          , IntentConfig "weather"
                         [ SlotConfig "city" RawE (keyboardMsg "В каком городе хотите узнать погоду?" ["В Казани", "В Москве", "В Питере"])
                            [ IntentRule (IntentRegexRule (R "В ([а-яА-Я]+)" [(1, RawE)])) ["city"]
                            , IntentRule (IntentRegexRule (R "В (городе [а-яА-Я]+)" [(1, RawE)])) ["city"]
                            ] Nothing ]
                         [ IntentRule (IntentRegexRule' (R "какая сегодня погода\\?" [])) []
                         , IntentRule (IntentRegexRule' (R "какая погода сейчас в ([а-яА-Я]+)\\?" [(1, RawE)])) ["city"]
                         , IntentRule (IntentRegexRule' (R "какая погода сейчас\\?$" [])) []
                         , IntentRule (IntentMLRule' (ML "weather"
                                ["Какая сейчас погода?", "Какая температура на улице?",
                                 "насколько холодно на улице" , "за окном тепло?"])) []
                         ]
                         []
                         Nothing
                         (\m -> return (noKeyboardMsg $ Text.concat ["Погода в ", formatEntity . fromJust $ Map.lookup "city" m, " приятная :)"]))
          , IntentConfig "ticket"
                         [ SlotConfig "dest" RawE "В какой город вы хотите поехать?"
                            [ IntentRule (IntentRegexRule (R "^(В )?([а-яА-Я]+)$" [(2, RawE)])) ["dest"]
                            ] Nothing
                         , SlotConfig "day" TimeE "В какой день вы хотите поехать?"
                            [ IntentRule (IntentRegexRule (R "(.+)" [(1, TimeE)])) ["day"]
                            ] (Just "Извините, не смог вас понять. Введите дату, когда вы хотите поехать.")
                         ]
                         [ IntentRule (IntentRegexRule' (R "^хочу купить билет в ([а-яА-Я]+)$" [(1, RawE)])) ["dest"]
                         , IntentRule (IntentRegexRule' (R "^купи мне билет в ([а-яА-Я]+)$" [(1, RawE)])) ["dest"]
                         , IntentRule (IntentRegexRule' (R "^купи мне билет на самолет$" [])) []
                         , IntentRule (IntentMLRule' (ML "ticket"
                                [ "купи билет на самолет", "хочу уехать из этого города"
                                , "хочу купить билет" , "купи мне билет", "нужно уехать"
                                , "купи мне билет на самолет", "купи мне билет на самолет 4 апреля 2017 года"
                                , "купи мне билет на самолет 10 июня 2019 года"
                                , "купи мне билет на самолет 9 июля 2018 года"
                                , "купи мне билет на самолет 9 мая 2018 года"
                                ])) ["day"]
                         ]
                         [ IntentRule (IntentRegexRule (R "в ([а-яА-Я]+) (.+)" [(1, RawE), (2, TimeE)])) ["dest", "day"]
                         ]
                         Nothing
                         (\m -> return (noKeyboardMsg $ Text.concat [ "Купил вам билет в ", formatEntity . fromJust $ Map.lookup "dest" m
                                                                    , ", дата: ", formatTimeEntity (fromJust $ Map.lookup "day" m) "%Y-%m-%d"
                                                                    ]))
          ]
