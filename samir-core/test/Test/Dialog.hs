module Test.Dialog
    ( dialogSpec
    ) where

import Test.Tasty.Hspec

import Control.Monad (void)
import Data.Text (Text)

import Samir.Bot
import Samir.Bot.Session
import Samir.Message

dialogs :: [(String, [(Text, Message)])]
dialogs = [ ("long", [ ("Привет", "Здравствуйте!")
                     , ("У меня есть сайт google.com", "Крутой сайт google.com!")
                     , ("Пока", "До свидания!")])
          , ("greeting", [("Привет", "Здравствуйте!")])
          , ("greeting 2", [("Приветствую", "Здравствуйте!")])
          , ("greeting 3", [("Приветики", "Здравствуйте!")])
          , ("goodbye", [("Пока", "До свидания!")])
          , ("goodbye 2", [("Прощай", "До свидания!")])
          , ("brb", [("Нужно отойти", "Буду ждать!")])
          , ("brb 2", [("Скоро вернусь", "Буду ждать!")])
          , ("fallback", [ ("Хохохо трололо ничего не поймешь xD", "Help Fallback")
                         , ("Это ты тоже не должен понять", "Help Fallback")
                         , ("О чем это я", "Help Fallback")
                         , ("Ну вот, все плохо", "Help Fallback")
                         ])
          , ("name", [("Меня зовут Азат", "Приятно познакомиться, Азат!")])
          , ("url", [("У меня есть сайт google.com", "Крутой сайт google.com!")])
          , ("url2", [("У меня есть сайт www.yandex.ru", "Крутой сайт www.yandex.ru!")])
          , ("weather", [ ("Какая погода сейчас?", keyboardMsg "В каком городе хотите узнать погоду?" ["В Казани", "В Москве", "В Питере"])
                        , ("В Казани", "Погода в Казани приятная :)")
                        ] )
          , ("weather 2", [ ("Какая погода сейчас в Казани?", "Погода в Казани приятная :)") ] )
          , ("weather fb", [ ("Какая погода сейчас?", keyboardMsg "В каком городе хотите узнать погоду?" ["В Казани", "В Москве", "В Питере"])
                           , ("lolal", "Извините, я не смог понять ваше последнее сообщение")
                           ] )
          , ("ticket", [ ("Купи мне билет", "В какой город вы хотите поехать?")
                       , ("В Казань", "В какой день вы хотите поехать?")
                       , ("15го апреля 2018 года", "Купил вам билет в Казань, дата: 2018-04-15")
                       ] )
          , ("ticket2", [ ("Купи мне билет", "В какой город вы хотите поехать?")
                        , ("В Казань 10 июня 2018 года", "Купил вам билет в Казань, дата: 2018-06-10")
                        ] )
          , ("ticket3", [ ("Хочу купить билет в Москву", "В какой день вы хотите поехать?")
                        , ("12 июня 2018 года", "Купил вам билет в Москву, дата: 2018-06-12")
                        ] )
          , ("ticket4", [ ("Купи мне билет", "В какой город вы хотите поехать?")
                        , ("В Казань", "В какой день вы хотите поехать?")
                        , ("Хаха, не поймешь", "Извините, не смог вас понять. Введите дату, когда вы хотите поехать.")
                        , ("15го апреля 2018 года", "Купил вам билет в Казань, дата: 2018-04-15")
                        ] )
          , ("ticket5", [ ("Купи мне билет", "В какой город вы хотите поехать?")
                        , ("В Казань", "В какой день вы хотите поехать?")
                        , ("За окном тепло?", keyboardMsg "В каком городе хотите узнать погоду?" ["В Казани", "В Москве", "В Питере"])
                        ] )
          , ("ticket6", [ ("Купи мне билет на самолет 18 мая 2018 года", "В какой город вы хотите поехать?")
                        , ("В Казань", "Купил вам билет в Казань, дата: 2018-05-18")
                        ] )
          ]

historySpec :: SamirBot -> Spec
historySpec bot = it "should retain history" $ fmap _ssHistory sess `shouldReturn` reverseHist
    where dialog = snd $ dialogs !! 1
          reverseHist = reverse . concatMap (\(x,y) -> [x,msgBody y]) $ dialog
          sess = sess' (newSession bot) dialog
          sess' s ((q,_):xs) = fst <$> getResponse s q >>= (`sess'` xs)
          sess' s _ = return s

dialogSpec :: SamirBot -> Spec
dialogSpec bot = describe "samir bot" $ historySpec bot >> void (traverse spec' dialogs)
    where spec' (n, msgs) = it ("should answer correctly in " ++ n) (spec'' (newSession bot) msgs)
          spec'' sess ((q,a):xs) = do
              (nsess, resp) <- getResponse sess q
              resp `shouldBe` a
              spec'' nsess xs
          spec'' _ _ = return ()
