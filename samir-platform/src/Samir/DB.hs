{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module Samir.DB
    ( getBotConfigs
    , BotId
    ) where

import Control.Monad.Logger (runStdoutLoggingT)
import Control.Monad.Reader (ReaderT(..))
import Data.ByteString (ByteString)
import Data.Text (Text)

import Database.Persist.Postgresql
import Database.Persist.Postgresql.JSON
import Database.Persist.TH

import Samir.Bot (BotConfig)

share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Bot
    name Text
    genericFallback Text
    helpFallback Text
    confidenceThreshold Double
    timezone Text
    lang Text
    intentConfigs Value
    mlClassifier ByteString Maybe
    UniqueName name
|]

connectionString :: ConnectionString
connectionString = "host=localhost port=5432 user=samir dbname=samir password=samir"

getBotConfigs :: IO [BotConfig]
getBotConfigs = runStdoutLoggingT $ withPostgresqlConn connectionString $ runReaderT $ do
    runMigration migrateAll
    return []
