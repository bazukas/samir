{-# LANGUAGE DeriveGeneric #-}
module Samir.Config
    ( Project(..)
    , Config(..)
    , loadConfig
    ) where

import Data.Aeson (genericParseJSON, defaultOptions)
import Data.Aeson.Types (fieldLabelModifier, camelTo2)
import Data.Text (Text)
import Data.Yaml (FromJSON(..), decodeFileEither)
import GHC.Generics (Generic)

import Samir.Logger

data Project = Project
    { pName :: Text
    , pClassifier :: FilePath
    , pRuleset :: FilePath
    , pTelegramToken :: Text
    } deriving (Generic, Show)

data Config = Config
    { cProjects :: [Project]
    , cVectorizer :: FilePath
    } deriving (Generic, Show)

instance FromJSON Project where
    parseJSON = genericParseJSON defaultOptions
        { fieldLabelModifier = camelTo2 '_' . drop 1 }
instance FromJSON Config where
    parseJSON = genericParseJSON defaultOptions
        { fieldLabelModifier = camelTo2 '_' . drop 1 }

component :: String
component = "Samir.Config"

configLocation :: FilePath
configLocation = "data/config.yaml"

loadConfig :: IO (Maybe Config)
loadConfig = do
    debugM component "Loading config..."
    eitherToMaybe <$> decodeFileEither configLocation
    where eitherToMaybe = either (const Nothing) id
