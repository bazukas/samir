module Main where

import Data.Maybe (fromJust)
import Data.Semigroup ((<>))
import Data.Text (Text)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Data.Time.LocalTime (TimeZone, hoursToTimeZone, getZonedTime)
import Options.Applicative
import System.IO (Handle, IOMode(..), BufferMode(..), openFile, hClose, hSetBuffering)
import Text.Printf
import qualified Data.Map.Strict as Map
import qualified Data.Text as Text

import Samir.Engine

data SamirOptions = SamirOptions
    { sVerbose :: Bool
    , sPort :: Int
    , sHost :: Text
    , sLocal :: Bool
    , sLogFile :: Maybe FilePath
    }

samirVerbose :: Parser Bool
samirVerbose = switch
    ( long "verbose"
    <> short 'v'
    <> help "Log debug messages")

samirLocal :: Parser Bool
samirLocal = switch
    ( long "local"
    <> short 'l'
    <> help "Start in local mode")

samirPort :: Parser Int
samirPort = option auto
    ( long "port"
    <> short 'p'
    <> help "HTTP Server port"
    <> showDefault
    <> value 8080
    <> metavar "INT")

samirHost :: Parser Text
samirHost = strOption
    ( long "host"
    <> short 'h'
    <> help "Host to bind webhooks to")

samirLogFile :: Parser (Maybe FilePath)
samirLogFile = optional $ strOption
    ( long "log-file"
    <> short 'f'
    <> help "File to log chat history")

samirOpts :: Parser SamirOptions
samirOpts = SamirOptions <$> samirVerbose <*> samirPort <*> samirHost <*> samirLocal <*> samirLogFile

main :: IO ()
main = do
    options <- execParser $ info (samirOpts <**> helper) fullDesc
    startBot options

logFile :: Handle -> SamirLogger
logFile h (p, u, txt, resp) = do
    t <- getZonedTime
    let ft = formatTime defaultTimeLocale "%Y-%m-%d %H:%M:%S" t
     in hPrintf h "%s - %s - %s\nMessage: %s\nResponse: %s\n" ft p (show u) txt (msgBody resp)

startBot :: SamirOptions -> IO ()
startBot (SamirOptions v p h l f) = do
    project <- samirProject l
    let ec = EngineConfig [project] v Nothing p h
     in case f of
          Nothing -> startSamirEngine (ec Nothing)
          Just f' -> do
              handle <- openFile f' AppendMode
              hSetBuffering handle LineBuffering
              startSamirEngine (ec (Just $ logFile handle))
              hClose handle

samirProject :: Bool -> IO Project
samirProject local = do
    params <- mlParams
    putStrLn "Training ML classifier..."
    mlClassifier <- buildMLClassifier params mskTz (bcIntents samirConfig)
    return Project
        { pName = "Samir hello project"
        , pId = "samir"
        , pBotConfig = samirConfig
        , pMLIntentClassifier = mlClassifier
        , pTelegramToken = if local then Just (TgLongPoll, "bot544422176:AAGfRW5hM2gnKNZ3mGpzyZWVt9HjwnWy6mM")
                                    else Just (TgCallback, "bot544422176:AAGfRW5hM2gnKNZ3mGpzyZWVt9HjwnWy6mM")
        , pVkToken = Just ("bcbb49ec5b020b07484a2404ce2749ebff4756a785ba7a797bdc07b4ddb47dca75a710f5f5bbefe395c0c", "da7df76b", "jklej0123kljs01kzpqwz")
        , pFbToken = Just (FbGateway "lkj2340sfkb092kjb0pqwelkn0234mvi"
                                     "EAAcGyICvt0YBAICouAW3EPihvsETUwRJJ8CcHId9oZCpMfuZCUYswwKJHqW9g5TXtZCOe7ev30rZCJ7CewG8Ex4Fik5gQxaxZCZAsDCM3NTZBhV2BNZCuBv0eox1BT60ZCUFf4MfbidC2rZBee9p0jhEEobSPdl1UC7TE5alwZB2gXS3wZDZD" "Здравствуйте" "")
        , pVbToken = if local then Nothing else Just ("Samir bot", "481aa735a267d09a-c91d58e1828a67df-e42f90aa15035ee2")
        }

mlParams :: IO MLParams
mlParams = do
    putStrLn "Loading vectorizer..."
    vectorizer <- loadVectorizer "../data/fasttext.ru.300.vec"
    putStrLn "Loading preset..."
    preset <- loadPreset "../data/mlpreset.yaml"
    return $ MLParams preset trainParams vectorizer
    where trainParams = MLTrainParams Nothing Nothing

mskTz :: TimeZone
mskTz = hoursToTimeZone 3

samirConfig :: BotConfig
samirConfig = BotConfig
    { bcIntents = samirIntents
    , bcGenericFallback = "Извините, я не смог понять ваше последнее сообщение"
    , bcHelpFallback = "Извините, я не смог понять ваше последнее сообщение"
    , bcThreshold = 0.05
    , bcTimeZone = mskTz
    , bcLang = RU
    }

samirIntents :: [IntentConfig]
samirIntents = [ IntentConfig "greeting" []
                              [ IntentRule (IntentRegexRule' (R "^(привет|здравствуйте)!?$" [])) []
                              , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.hello") []
                              , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodmoring") []
                              , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodeveing") []
                              , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.goodnight") []
                              ]
                              []
                              Nothing
                              (staticAnswer ["Здравствуйте!"])
               , IntentConfig "goodbye" []
                              [ IntentRule (IntentRegexRule' (R "^пока!?$" [])) []
                              , IntentRule (IntentPresetMLRule' "samir.smalltalk.greetings.bye") []
                              ]
                              []
                              Nothing
                              (staticAnswer ["До свидания!"])
               , IntentConfig "brb" []
                              [ IntentRule (IntentPresetMLRule' "samir.smalltalk.user.will_be_back") []
                              ]
                              []
                              Nothing
                              (staticAnswer ["Буду ждать!"])
               , IntentConfig "thankyou" []
                              [ IntentRule (IntentPresetMLRule' "samir.smalltalk.appraisal.thank_you") []
                              ]
                              []
                              Nothing
                              (staticAnswer ["Не за что :)"])
               , IntentConfig "ticket"
                              [ SlotConfig "dest" RawE "В какой город вы хотите поехать?"
                                 [ IntentRule (IntentRegexRule (R "^(В )?([а-яА-Я]+)$" [(2, RawE)])) ["dest"]
                                 ] Nothing
                              , SlotConfig "day" TimeE "В какой день вы хотите поехать?"
                                 [ IntentRule (IntentRegexRule (R "(.+)" [(1, TimeE)])) ["day"]
                                 ] (Just "Извините, не смог вас понять. Введите дату, когда вы хотите поехать.")
                              ]
                              [ IntentRule (IntentRegexRule' (R "^хочу купить билет в ([а-яА-Я]+)$" [(1, RawE)])) ["dest"]
                              , IntentRule (IntentRegexRule' (R "^купи мне билет в ([а-яА-Я]+)$" [(1, RawE)])) ["dest"]
                              , IntentRule (IntentRegexRule' (R "^купи мне билет на самолет$" [])) []
                              , IntentRule (IntentMLRule' (ML "ticket"
                                     [ "купи билет на самолет", "хочу уехать из этого города"
                                     , "хочу купить билет" , "купи мне билет", "нужно уехать"
                                     , "купи мне билет на самолет", "купи мне билет на самолет 4 апреля 2017 года"
                                     , "купи мне билет на самолет 10 июня 2019 года"
                                     , "купи мне билет на самолет 9 июля 2018 года"
                                     , "купи мне билет на самолет 9 мая 2018 года"
                                     ])) ["day"]
                              ]
                              [ IntentRule (IntentRegexRule (R "в ([а-яА-Я]+) (.+)" [(1, RawE), (2, TimeE)])) ["dest", "day"]
                              ]
                              Nothing
                              (\m -> return $ noKeyboardMsg (Text.concat [ "Купил вам билет в ", formatEntity . fromJust $ Map.lookup "dest" m
                                                                         , ", дата: ", formatTimeEntity (fromJust $ Map.lookup "day" m) "%Y-%m-%d"
                                                                         ]))
               , IntentConfig "weather"
                              [ SlotConfig "city" RawE (keyboardMsg "В каком городе хотите узнать погоду?"
                                                            [ "В Казани", "В Москве", "В Питере"
                                                            , "В Набережных челнах", "В Самаре", "В Сочи"])
                                 [ IntentRule (IntentRegexRule (R "В ([а-яА-Я]+)" [(1, RawE)])) ["city"]
                                 , IntentRule (IntentRegexRule (R "В (городе [а-яА-Я]+)" [(1, RawE)])) ["city"]
                                 ] Nothing ]
                              [ IntentRule (IntentRegexRule' (R "какая сегодня погода\\?" [])) []
                              , IntentRule (IntentRegexRule' (R "какая погода сейчас в ([а-яА-Я]+)\\?" [(1, RawE)])) ["city"]
                              , IntentRule (IntentRegexRule' (R "какая погода сейчас\\?$" [])) []
                              , IntentRule (IntentMLRule' (ML "weather"
                                     ["Какая сейчас погода?", "Какая температура на улице?",
                                      "насколько холодно на улице" , "за окном тепло?"])) []
                              ]
                              []
                              Nothing
                              (\m -> return (noKeyboardMsg $ Text.concat
                                ["Погода в ", formatEntity . fromJust $ Map.lookup "city" m, " приятная :)"]))
               ]
