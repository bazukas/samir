module Samir.Engine.Supervisor
    ( supervise
    , runParallel
    ) where

import Control.Concurrent (threadDelay)
import Control.Concurrent.Async (Async, async, poll, uninterruptibleCancel)
import Control.Exception (finally)
import Control.Exception.Base (displayException)
import Control.Monad (void)
import Data.Foldable (traverse_)
import Data.IORef (IORef, newIORef, readIORef, atomicWriteIORef)
import Data.Text (Text, pack)

import Samir.Logger

newtype Supervisor = Supervisor
    { _supDaemons :: IORef [(Async (), IO (), Text)] }

checkAndRestartAsync :: (Async (), IO (), Text) -> IO (Async (), IO (), Text)
checkAndRestartAsync (as, a, n) = do
    res <- poll as
    case res of
      Nothing -> return (as, a, n)
      Just e -> do
          let msg = case e of
                      Left ex -> ["Thread for ", n, " exited with exception: ", pack (displayException ex), ", restarting..."]
                      Right _ -> ["Thread for ", n, " exited, restarting..."]
          warningL "Samir.Engine.Supervisor" msg
          as' <- async a
          return (as', a, n)


checkAsyncsLoop :: Int -> Supervisor -> IO ()
checkAsyncsLoop checkInterval sup@(Supervisor asyncsRef) = do
    threadDelay (checkInterval * 1000000)
    asyncs <- readIORef asyncsRef
    asyncs' <- traverse checkAndRestartAsync asyncs
    atomicWriteIORef asyncsRef asyncs'                       -- TODO: a stray thread may be left if parrent thread interrupted here
    checkAsyncsLoop checkInterval sup

cancelAsyncs :: Supervisor -> IO ()
cancelAsyncs (Supervisor asyncsRef) = do
    asyncs <- (fmap . fmap) (\(a,_,_) -> a) (readIORef asyncsRef)
    void $ traverse uninterruptibleCancel asyncs

supervise :: Int -> [(IO (), Text)] -> IO ()
supervise checkInterval daemons = do
    asyncs <- traverse (async . addLog) daemons
    sup <- newSup asyncs daemons
    checkAsyncsLoop checkInterval sup `finally` cancelAsyncs sup     -- For some reason check asyncs loop exited,
                                                                     -- cancelling children threads
    where newSup as da = Supervisor <$> newIORef (zipWith (\a (b, c) -> (a, b, c)) as da)
          addLog (a, n) = infoL "Samir.Engine.Supervisor" ["Supervising ", n] >> a

-- TODO check for errors
runParallel :: [IO ()] -> IO ()
runParallel = traverse_ async
