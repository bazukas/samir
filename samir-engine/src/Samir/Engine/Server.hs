module Samir.Engine.Server
    ( RequestHandler
    , RequestMap
    , startServer
    ) where

import Control.Concurrent (forkIO, threadDelay)
import Control.Monad (void)
import Data.Text (Text, intercalate)
import Network.Wai (Application, responseLBS, pathInfo)
import Network.HTTP.Types (status404)
import Network.Wai.Handler.Warp (run)
import qualified Data.Map.Strict as Map

import Samir.Engine.Supervisor
import Samir.Logger

type RequestHandler = Application
type RequestMap = Map.Map Text RequestHandler

serverApp :: RequestMap -> Application
serverApp rm req respond = case Map.lookup path rm of
                             Nothing -> respond $ responseLBS status404 [] ""
                             Just h -> h req respond
    where path = intercalate "/" $ pathInfo req

schedulePostActions :: [IO ()] -> IO ()
schedulePostActions actions = void $ forkIO $ do
    threadDelay (2 * 1000000)
    runParallel actions

-- TODO: come up with something better
startServer :: Int -> RequestMap -> [IO ()] -> IO ()
startServer port rm post = do
    infoM "Samir.Engine.Server" "Initializing http server"
    schedulePostActions post
    run port (serverApp rm)
