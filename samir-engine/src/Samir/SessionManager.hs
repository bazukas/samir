module Samir.SessionManager
    ( SessionManager
    , SamirLogger
    , buildSessionManager
    , sessionResponse
    ) where

import Data.IORef (IORef, newIORef, atomicModifyIORef', readIORef)
import Data.Text (Text)
import Data.Time.Clock (UTCTime, NominalDiffTime, getCurrentTime, addUTCTime)
import qualified Data.Map.Strict as Map

import Samir.Bot
import Samir.Bot.Session
import Samir.Message

import Samir.Gateway.Types

type SamirLogger = (Text, UserID, Text, Message) -> IO ()

data SessionManager = SessionManager
    { _smProject :: Text
    , _smBot :: SamirBot
    , _smMap :: IORef (Map.Map UserID (SamirSession, UTCTime))
    , _smLogger :: Maybe SamirLogger
    , _smBuffer :: IORef [MessageID]
    }

buildSessionManager :: Text -> SamirBot -> Maybe SamirLogger -> IO SessionManager
buildSessionManager n b l = SessionManager n b <$> newIORef Map.empty <*> pure l <*> newIORef []

-- 5 minutes
sessionExpiration :: NominalDiffTime
sessionExpiration = 5 * 60

bufferMaxLength :: Int
bufferMaxLength = 500

bufferConcatLength :: Int
bufferConcatLength = 100

getSession :: SessionManager -> UserID -> IO (SamirSession, UTCTime)
getSession (SessionManager _ b iom _ _) userId = do
    m <- readIORef iom
    currentTime <- getCurrentTime
    case Map.lookup userId m of
      Nothing -> return (newSession b, currentTime)
      Just (sess, lastMessageTime) -> if addUTCTime sessionExpiration lastMessageTime < currentTime
                                         then return (newSession b, currentTime)
                                         else return (sess, currentTime)

-- TODO: optimize
isDuplicate :: SessionManager -> MessageID -> IO Bool
isDuplicate (SessionManager _ _ _ _ iob) msgId = atomicModifyIORef' iob $ \b ->
    if msgId `elem` b then (b, True)
                      else if length b >= bufferMaxLength then (take bufferConcatLength (msgId:b), False)
                                                          else (msgId:b, False)

sessionResponse' :: SessionManager -> Text -> UserID -> IO Message
sessionResponse' sm@(SessionManager _ _ iom _ _) txt userId = do
    (sess, currentTime) <- getSession sm userId
    (sess', resp) <- getResponse sess txt
    atomicModifyIORef' iom (\m -> (Map.insert userId (sess', currentTime) m, ()))
    return resp

sessionResponse :: SessionManager -> MessageHandler
sessionResponse sm@(SessionManager n _ _ logger _) (txt, userId, messageId) = do
    dup <- isDuplicate sm messageId
    if dup then return Nothing
           else do
               resp <- sessionResponse' sm txt userId
               case logger of
                 Nothing -> return ()
                 Just logger' -> logger' (n, userId, txt, resp)
               return (Just resp)
