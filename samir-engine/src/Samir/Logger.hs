module Samir.Logger
    ( debugM
    , infoM
    , warningM
    , errorM
    , exceptionM
    , debugL
    , infoL
    , warningL
    , errorL
    , initLogging
    ) where

import Control.Exception (Exception, displayException)
import Data.Text (Text, concat, unpack, pack)
import System.IO (stdout)

import System.Log.Formatter
import System.Log.Logger (Priority(..), logM, updateGlobalLogger, rootLoggerName, removeHandler
                         , setLevel, setHandlers)
import System.Log.Handler (setFormatter)
import System.Log.Handler.Simple (verboseStreamHandler)

logM' :: String -> Priority -> Text -> IO ()
logM' c p t = logM c p (unpack t)

logL' :: String -> Priority -> [Text] -> IO ()
logL' c p t = logM' c p (Data.Text.concat t)

debugM :: String -> Text -> IO ()
debugM c = logM' c DEBUG

infoM :: String -> Text -> IO ()
infoM c = logM' c INFO

warningM :: String -> Text -> IO ()
warningM c = logM' c WARNING

errorM :: String ->  Text -> IO ()
errorM c = logM' c ERROR

debugL :: String ->  [Text] -> IO ()
debugL c = logL' c DEBUG

infoL :: String ->  [Text] -> IO ()
infoL c = logL' c INFO

warningL :: String ->  [Text] -> IO ()
warningL c = logL' c WARNING

errorL :: String ->  [Text] -> IO ()
errorL c = logL' c ERROR

exceptionM :: Exception e => String -> Text -> e -> IO ()
exceptionM c txt e = logL' c ERROR [txt, pack $ displayException e]

initLogging :: Bool -> IO ()
initLogging verbose = do
    let p = if verbose then DEBUG else INFO
        fmt = simpleLogFormatter "$time - [$loggername/$prio] - $msg"
    updateGlobalLogger rootLoggerName removeHandler
    h <- (`setFormatter` fmt) <$> verboseStreamHandler stdout p
    updateGlobalLogger "Samir" (setLevel p . setHandlers [h])
