module Samir.Engine
    ( EngineConfig(..)
    , Project(..)
    , SamirLogger
    , loadPreset
    , startSamirEngine
    , module Samir.Bot
    , module Samir.Gateway
    , module Samir.Lang
    , module Samir.Message
    ) where

import Control.Exception (Exception)
import Data.Maybe (catMaybes, fromMaybe)
import Data.Monoid ((<>))
import Data.String (fromString)
import Data.Text (Text, intercalate)
import Data.Yaml (decodeFileEither, ParseException)
import qualified Control.Exception as E
import qualified Data.Map.Strict as Map

import Samir.Bot
import Samir.Lang
import Samir.Message

import Samir.Engine.Server
import Samir.Engine.Supervisor
import Samir.Gateway
import Samir.Logger
import Samir.SessionManager

data Project = Project
    { pName :: Text
    , pId :: Text
    , pBotConfig :: BotConfig
    , pMLIntentClassifier :: MLIntentClassifier
    , pTelegramToken :: Maybe (TgAPI, Text)
    , pVkToken :: Maybe (Text, Text, Text)
    , pFbToken :: Maybe FbGateway
    , pVbToken :: Maybe (Text, Text)
    }

data EngineConfig = EngineConfig
    { _ecProjects :: [Project]
    , _ecVerbose :: Bool
    , _ecSuperviseInterval :: Maybe Int
    , _ecServerPort :: Int
    , _ecHost :: Text
    , _ecLogger :: Maybe SamirLogger
    }

loadPreset :: FilePath -> IO PresetMLData
loadPreset fp = fmap (PresetMLData . etom) (decodeFileEither fp)
    where etom :: Either ParseException (Map.Map String [Text]) -> Map.Map MLClass [Text]
          etom (Left _) = Map.fromList []
          etom (Right m) = Map.mapKeys fromString m

defaultInterval :: Int
defaultInterval = 60

startProject :: Text -> Int -> Project -> Maybe SamirLogger -> IO (RequestMap, IO (), [IO ()])
startProject host interval (Project n i bc ml mt mvk mfb mvb) logger = do
    sm <- buildSessionManager n (buildSamirBot ml bc) logger
    let gws = gateways sm
     in return (requestMap gws, start' gws, post gws)
    where start' gws = do
            infoL "Samir.Engine" ["Starting project ", n]
            supervise interval (catMaybes $ fmap gatewayF gws)
          gatewayF gw = (\d -> (d, gwName gw <> " gateway daemon for " <> n)) <$> gwDaemon gw
          gateways sm = catMaybes [ fmap (telegramGateway (sessionResponse sm)) mt
                                  , fmap (vkGateway (sessionResponse sm)) mvk
                                  , fmap (fbGateway (sessionResponse sm)) mfb
                                  , fmap (viberGateway (sessionResponse sm)) mvb
                                  ]
          requestMap = Map.unions . fmap getHooks
          getHooks gw = Map.mapKeys (\r -> intercalate "/" [i, gwName gw, r]) (gwHooks gw)
          post gws = fmap (\(a, gwn) -> a (host' gwn)) $ catMaybes $ fmap (\g -> (,) <$> gwBindWebhooks g <*> pure (gwName g)) gws
          host' gwn = "https://" <> intercalate "/" [host, i, gwn]

startSamirEngine' :: EngineConfig -> IO ()
startSamirEngine' (EngineConfig projects verbose mInterval port host logger) = do
    initLogging verbose
    infoM "Samir.Engine" "Initializing Samir engine"
    (requestMaps, projectInfos, postActions) <- unzip3 <$> traverse projectF projects
    supervise interval (projectInfos ++ [(startServer port (Map.unions requestMaps) (concat postActions), "Samir engine http server")])
    where projectF p = fmap (\(a,b,c) -> (a, (b, "Project " <> pName p), c)) (startProject host interval p logger)
          interval = fromMaybe defaultInterval mInterval

startSamirEngine :: EngineConfig -> IO ()
startSamirEngine c = startSamirEngine' c `E.catches` [ E.Handler handleAsync
                                                     , E.Handler handleSome
                                                     ]
    where handleAsync :: E.AsyncException -> IO ()
          handleAsync E.UserInterrupt = infoM "Samir.Engine" "Engine interrupted by user"
          handleAsync ex = handleUncaught ex
          handleSome :: E.SomeException -> IO ()
          handleSome = handleUncaught
          handleUncaught :: Exception e => e -> IO ()
          handleUncaught = exceptionM "Samir.Engine" "Uncaught exception while running engine: "
