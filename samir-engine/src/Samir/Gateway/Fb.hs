{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
module Samir.Gateway.Fb
    ( FbGateway(..)
    , fbGateway
    ) where

import Control.Monad (void, when)
import Control.Monad.IO.Class (liftIO)
import Data.Proxy (Proxy(..))
import Data.Text (Text)
import Formatting ((%), sformat, stext)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant
import Web.FBMessenger.API.Bot
import qualified Data.Map.Strict as Map

import Samir.Engine.Server (RequestHandler)
import Samir.Gateway.Types
import Samir.Logger
import Samir.Message
webhookPath :: Text
webhookPath = "webhook"

type WebHookAPI' = QueryParam "hub.verify_token" Text :> QueryParam "hub.challenge" Text :> Get '[PlainText] Text
              :<|> ReqBody '[JSON] RemoteEventList :> Post '[PlainText, JSON] Text

type WebHookAPI = Capture "project_name" Text :> Capture "gw_name" Text :> "webhook" :> WebHookAPI'

webHookAPI :: Proxy WebHookAPI
webHookAPI = Proxy

fbUserID :: Text -> UserID
fbUserID u = UserID $ sformat ("fb:" % stext) u

fbMessageID :: Text -> MessageID
fbMessageID u = MessageID $ sformat ("fb:" % stext) u

getStartedPayload :: Text
getStartedPayload = "GET_STARTED"

webhook :: MessageHandler -> FbGateway -> RequestHandler
webhook mh fbg = serve webHookAPI webhook'
    where webhook' _ _ = webhookVerify :<|> webhookMessage
          webhookVerify (Just verifyToken) (Just challenge)
            | verifyToken == fbVerifyToken fbg = return challenge
          webhookVerify _ _ = throwError err403
          webhookMessage (RemoteEventList res) = do
              void $ liftIO $ traverse handleMessage (concatMap evt_messaging res)
              return "{\"status\":\"fulfilled\"}"
          handleMessage EventMessage{evtSenderId=senderId, evtContent=(EmTextMessage msgId _ msg)} = do
              resp <- mh (msg, fbUserID senderId, fbMessageID msgId)
              void $ traverse (sendMessage senderId) resp
          handleMessage EventMessage{evtSenderId=senderId, evtContent=(EmPostback p)} =
              when (p == getStartedPayload) (sendMessage senderId (noKeyboardMsg $ fbWelcomeMessage fbg))
          handleMessage _ = return ()
          sendMessage u msg = do
              m <- newManager tlsManagerSettings
              let (Just rcpt) = recipient (Just u) Nothing
              res <- let messageReq = sendTextMessageRequest Nothing rcpt (msgBody msg) (quickReplies msg)
                      in sendTextMessage (Just . Token $ fbAccessToken fbg) messageReq m
              case res of
                Left err -> exceptionM "Samir.Gateway.Fb" "Error while sending message: " err
                Right _ -> return ()

quickReplies :: Message -> Maybe [QuickReply]
quickReplies msg = (fmap . fmap) (\t -> TextQuickReply t t Nothing) (msgKeyboard msg)

setGetStarted :: FbGateway -> IO ()
setGetStarted fbg = do
    m <- newManager tlsManagerSettings
    res <- setGetStartedButton token (GetStartedRequest getStartedPayload) m
    case res of
      Left err -> exceptionM "Samir.Gateway.Fb" "Error while setting get started button: " err
      Right _ -> return ()
    res' <- setGreetingText token (setDefaultLocaleGreetingRequest (fbGreetingText fbg)) m
    case res' of
      Left err -> exceptionM "Samir.Gateway.Fb" "Error while setting greeting text: " err
      Right _ -> return ()
    where token = Just . Token . fbAccessToken $ fbg

data FbGateway = FbGateway
    { fbVerifyToken :: Text
    , fbAccessToken :: Text
    , fbWelcomeMessage :: Text
    , fbGreetingText :: Text
    }

fbGateway :: MessageHandler -> FbGateway -> Gateway
fbGateway mh fbg = Gateway "fb" Nothing (Map.fromList [(webhookPath, webhook mh fbg)])
                            (Just $ const $ setGetStarted fbg)
