{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeOperators #-}
module Samir.Gateway.Vk
    ( vkGateway
    ) where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson (Array, FromJSON(..), ToJSON(..), encode, withArray, withObject, object, (.:), (.=))
import Data.Aeson.Types (Parser, Value(..))
import Data.ByteString.Lazy (toStrict)
import Data.Text (Text, pack)
import Data.Text.Encoding (decodeUtf8)
import Formatting ((%), sformat, int)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant
import Servant.Client (BaseUrl(..), Scheme(..), ClientM, ServantError, client, runClientM, mkClientEnv)
import System.Random (randomRIO)
import Web.Internal.HttpApiData (ToHttpApiData)
import qualified Data.Map.Strict as Map
import qualified Data.Vector as V

import Samir.Engine.Server (RequestHandler)
import Samir.Gateway.Types
import Samir.Logger
import Samir.Message

component :: String
component = "Samir.Gateway.Vk"

vkUserID :: Integer -> UserID
vkUserID u = UserID $ sformat ("vk:" % int) u

vkMessageId :: Integer -> MessageID
vkMessageId u = MessageID $ sformat ("vk:" % int) u

-- VK Api methods

newtype VkToken = VkToken Text
    deriving ToHttpApiData

data GetLPSResponse = GetLPSResponse
    { _lpsKey :: Text
    , _lpsServer :: String
    , _lpsTs :: Integer
    }
instance FromJSON GetLPSResponse where
    parseJSON = withObject "obj" $ \o -> do
        res <- o .: "response"
        key <- res .: "key"
        server <- res .: "server"
        ts <- res .: "ts"
        return $ GetLPSResponse key server ts

data Update = Message Integer Integer Text | OtherUpdate
    deriving Show
instance FromJSON Update where
    parseJSON = withArray "arr" parseUpdate
        where parseUpdate :: Array -> Parser Update
              parseUpdate arr
                | V.null arr = fail "update array is empty"
                | V.head arr == Number 4 && V.length arr >= 6 = do
                    user_id <- parseJSON (arr V.! 3)
                    msg <- parseJSON (arr V.! 5)
                    flags <- parseJSON (arr V.! 2)
                    return $ Message user_id flags msg
                | otherwise = return OtherUpdate

data GetUpdatesResponse = GetUpdatesResponse
    { _guTs :: Integer
    , _guUpdates :: [Update]
    }
instance FromJSON GetUpdatesResponse where
    parseJSON = withObject "obj" $ \o ->
        GetUpdatesResponse <$> o .: "ts" <*> o .: "updates"

newtype MarkAsReadResponse = MarkAsReadResponse Int
    deriving (Eq, Show, Num)
instance FromJSON MarkAsReadResponse where
    parseJSON = withObject "obj" $ \o -> MarkAsReadResponse <$> o .: "response"

newtype SendMessageResponse = SendMessageResponse Int
instance FromJSON SendMessageResponse where
    parseJSON = withObject "obj" $ \o -> SendMessageResponse <$> o .: "response"

data VkColor = VkDefault | VkPrimary | VkNegative | VkPositive
instance ToJSON VkColor where
    toJSON VkDefault = "default"
    toJSON VkPrimary = "primary"
    toJSON VkNegative = "negative"
    toJSON VkPositive = "positive"

data VkButton = VkButton
    { _vkLabel :: Text
    , _vkColor :: VkColor
    }
instance ToJSON VkButton where
    toJSON (VkButton l c) = object ["action" .= object ["type" .= ("text" :: Text), "label" .= l], "color" .= c]

data VkKeyboard = VkKeyboard
    { _vkOneTime :: Bool
    , _vkButtons :: [[VkButton]]
    }
instance ToHttpApiData VkKeyboard where
    toQueryParam = decodeUtf8 . toStrict . encode
instance ToJSON VkKeyboard where
    toJSON (VkKeyboard ot bs) = object ["one_time" .= ot, "buttons" .= bs]

type RQueryParam = QueryParam' '[Required, Strict]

type VkAPI' a = RQueryParam "v" Text :> RQueryParam "access_token" VkToken :> "method" :> a
type MarkAsReadAPI = "messages.markAsRead" :> RQueryParam "peer_id" Integer :> Get '[JSON] MarkAsReadResponse
type SendMessageAPI = "messages.send" :> RQueryParam "random_id" Int :> RQueryParam "peer_id" Integer :>
                    RQueryParam "message" Text :> QueryParam "keyboard" VkKeyboard :> Get '[JSON] SendMessageResponse
type VkAPI = VkAPI' MarkAsReadAPI :<|> VkAPI' SendMessageAPI

vkVersion :: Text
vkVersion = "5.80"

vkAPI :: Proxy VkAPI
vkAPI = Proxy

markAsRead' :: Text -> VkToken -> Integer -> ClientM MarkAsReadResponse
sendMessage' :: Text -> VkToken -> Int -> Integer -> Text -> Maybe VkKeyboard -> ClientM SendMessageResponse
markAsRead' :<|> sendMessage' = client vkAPI

vkBaseUrl :: BaseUrl
vkBaseUrl = BaseUrl Https "api.vk.com" 443 ""

markAsRead :: VkToken -> Integer -> IO (Either ServantError MarkAsReadResponse)
markAsRead t user_id = do
    m <- newManager tlsManagerSettings
    runClientM (markAsRead' vkVersion t user_id) (mkClientEnv m vkBaseUrl)

sendMessage :: VkToken -> Integer -> Message -> IO (Either ServantError SendMessageResponse)
sendMessage t user_id msg = do
    m <- newManager tlsManagerSettings
    random_id <- liftIO . randomRIO $ (1, 100000)
    runClientM (sendMessage' vkVersion t random_id user_id (msgBody msg) (vkKeyboard msg)) (mkClientEnv m vkBaseUrl)

vkKeyboard :: Message -> Maybe VkKeyboard
vkKeyboard msg = case msgKeyboard msg of
                   Nothing -> Nothing
                   Just bs -> Just $ VkKeyboard True (fmap vkButton bs)
    where vkButton l = [VkButton l VkDefault]

-- Webhook API

data VkCallback' = ConfirmationCallback
                 | MessageCallback Integer Integer Text
                 | OtherCallback
data VkCallback = VkCallback Text VkCallback'

instance FromJSON VkCallback where
    parseJSON = withObject "obj" $ \o -> do
        t <- o .: "type"
        s <- o .: "secret"
        cb <- case t of
          String "confirmation" -> return ConfirmationCallback
          String "message_new" -> do
              o' <- o .: "object"
              userId <- o' .: "user_id"
              body <- o' .: "body"
              out <- o' .: "out"
              messageId <- o' .: "id"
              if out == (0 :: Int) then return $ MessageCallback userId messageId body
                                   else return OtherCallback
          _ -> return OtherCallback
        return $ VkCallback s cb

webhookPath :: Text
webhookPath = "webhook"

type WebHookAPI' = ReqBody '[JSON] VkCallback :> Post '[PlainText] Text
type WebHookAPI = Capture "project_name" Text :> Capture "gw_name" Text :> "webhook" :> WebHookAPI'

webHookAPI :: Proxy WebHookAPI
webHookAPI = Proxy

webhook :: MessageHandler -> (VkToken, Text, Text) -> RequestHandler
webhook mh (t, v, s) = serve webHookAPI (\_ _ -> webhook')
    where webhook' (VkCallback secret cb)
            | secret == s = webhook'' cb
            | otherwise = throwError err403
          webhook'' ConfirmationCallback = return v
          webhook'' OtherCallback = return "ok"
          webhook'' (MessageCallback userId messageId txt) = do
              liftIO $ handleMessage userId messageId txt
              return "ok"
          handleMessage userId messageId txt = do
              rres <- markAsRead t userId
              case rres of
                Left err -> exceptionM component "Encountered error while marking as read: " err
                Right 1 -> return ()
                Right r -> warningL component ["Unexpected response for marking as read: ", pack . show $ r]
              resp <- mh (txt, vkUserID userId, vkMessageId messageId)
              case resp of
                Nothing -> return ()
                Just resp' -> do
                    res <- sendMessage t userId resp'
                    case res of
                      Left err -> exceptionM component "Encountered error while sending message: " err
                      Right _ -> return ()

vkGateway :: MessageHandler -> (Text, Text, Text) -> Gateway
vkGateway mh (t, v, s) = Gateway "vk" Nothing (Map.fromList [(webhookPath, webhook mh (VkToken t, v, s))]) Nothing
