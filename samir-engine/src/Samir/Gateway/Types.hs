module Samir.Gateway.Types
    ( UserID(..)
    , MessageID(..)
    , MessageHandler
    , Gateway(..)
    ) where

import Data.Text (Text)

import Samir.Engine.Server (RequestMap)
import Samir.Message

newtype UserID = UserID Text
    deriving (Eq, Ord, Show)

newtype MessageID = MessageID Text
    deriving (Eq, Ord, Show)

type MessageHandler = (Text, UserID, MessageID) -> IO (Maybe Message)

data Gateway = Gateway
    { gwName :: Text
    , gwDaemon :: Maybe (IO ())
    , gwHooks :: RequestMap
    , gwBindWebhooks :: Maybe (Text -> IO ())
    }
