{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeOperators #-}
module Samir.Gateway.Viber
    ( viberGateway
    ) where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson (FromJSON(..), ToJSON(..), object, withObject, withText, (.=), (.:), (.:?))
import Data.Monoid ((<>))
import Data.Text (Text)
import Formatting ((%), sformat, stext, int)
import Network.HTTP.Client (newManager)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Servant
import Servant.Client (BaseUrl(..), Scheme(..), ClientM, client, runClientM, mkClientEnv)
import qualified Data.Map.Strict as Map

import Samir.Engine.Server (RequestHandler)
import Samir.Gateway.Types
import Samir.Logger
import Samir.Message

newtype ViberToken = ViberToken Text
    deriving ToHttpApiData

component :: String
component = "Samir.Gateway.Viber"

-- Viber API

data SetWebhookRequest = SetWebhookRequest
    { _reqUrl :: Text
    , _reqEventTypes :: [ViberEvent]
    , _reqSendName :: Bool
    , _reqSendPhoto :: Bool
    }
instance ToJSON SetWebhookRequest where
    toJSON (SetWebhookRequest u et n p) = object
        [ "url" .= u
        , "event_types" .= et
        , "send_name" .= n
        , "send_photo" .= p
        ]

data SetWebhookResponse = SetWebhookResponse
    { _resStatus :: Int
    , _resStatusMessage :: Text
    , _resEventTypes :: [ViberEvent]
    }
instance FromJSON SetWebhookResponse where
    parseJSON = withObject "object" $ \o ->
        SetWebhookResponse <$> o .: "status" <*> o .: "status_message" <*> o .: "event_types"

newtype Sender = Sender
    { _sName :: Text
    }
instance ToJSON Sender where
    toJSON (Sender n) = object
        [ "name" .= n
        ]

data ViberButton = ViberButton
    { _vbActionBody :: Text
    , _vbText :: Text
    }
instance ToJSON ViberButton where
    toJSON (ViberButton ab t) = object [ "ActionBody" .= ab, "Text" .= t ]

data ViberKeyboard = ViberKeyboard
    { _kbBgColor :: Maybe Text
    , _kbButtons :: [ViberButton]
    }
instance ToJSON ViberKeyboard where
    toJSON (ViberKeyboard bg bs) = object [ "BgColor" .= bg, "Buttons" .= bs ]

data SendMessageRequest = SendMessageRequest
    { _sendReqReceiver :: Text
    , _sendReqSender :: Sender
    , _sendReqText :: Text
    , _sendApiVersion :: Int
    , _sendKeyboard :: Maybe ViberKeyboard
    }
instance ToJSON SendMessageRequest where
    toJSON (SendMessageRequest r s t v kb) = object
        [ "receiver" .= r
        , "sender" .= s
        , "type" .= ("text" :: Text)
        , "text" .= t
        , "min_api_version" .= v
        , "keyboard" .= kb
        ]

data SendMessageResponse = SendMessageResponse
    { _sendResMessageToken :: Integer
    , _sendResStatus :: Int
    , _sendResStatusMessage :: Text
    }
instance FromJSON SendMessageResponse where
    parseJSON = withObject "object" $ \o ->
        SendMessageResponse <$> o .: "message_token" <*> o .: "status" <*> o .: "status_message"

type ViberAPI' a = Header' '[Required, Strict] "X-Viber-Auth-Token" ViberToken :> "pa" :> a
type SetWebhookAPI = "set_webhook" :> ReqBody '[JSON] SetWebhookRequest :> Post '[JSON] SetWebhookResponse
type SendMessageAPI = "send_message" :> ReqBody '[JSON] SendMessageRequest :> Post '[JSON] SendMessageResponse
type ViberAPI = ViberAPI' SetWebhookAPI
           :<|> ViberAPI' SendMessageAPI

viberAPI :: Proxy ViberAPI
viberAPI = Proxy

setWebHook :: ViberToken -> SetWebhookRequest -> ClientM SetWebhookResponse
sendMessage :: ViberToken -> SendMessageRequest -> ClientM SendMessageResponse
setWebHook :<|> sendMessage = client viberAPI

viberBaseUrl :: BaseUrl
viberBaseUrl = BaseUrl Https "chatapi.viber.com" 443 ""

bindWebhook :: ViberToken -> Text -> IO ()
bindWebhook t host = do
    debugM component "Setting Viber webhook"
    m <- newManager tlsManagerSettings
    res <- runClientM (setWebHook t (SetWebhookRequest url [] False False)) (mkClientEnv m viberBaseUrl)
    case res of
      Left err -> exceptionM component "Received error when setting webhook: " err
      Right (SetWebhookResponse s sm _)
        | s /= 0 -> errorL component ["Received error when setting webhook: ", sm]
      _ -> return ()
    where url = host <> "/" <> webhookPath

-- Webhook API

webhookPath :: Text
webhookPath = "webhook"

type WebHookAPI' = ReqBody '[JSON] ViberCallback :> Post '[PlainText] Text
type WebHookAPI = Capture "project_name" Text :> Capture "gw_name" Text :> "webhook" :> WebHookAPI'

newtype MessageSender = MessageSender
    { _msId :: Text
    }
instance FromJSON MessageSender where
    parseJSON = withObject "object" $ \o ->
        MessageSender <$> o .: "id"

data MessageType = VText | VPicture | VVideo | VFile | VSticker | VContact | VURL | VLocation
instance FromJSON MessageType where
    parseJSON  = withText "event" $ \case
        "text" -> return VText
        "picture" -> return VPicture
        "video" -> return VVideo
        "file" -> return VFile
        "sticker" -> return VSticker
        "contact" -> return VContact
        "url" -> return VURL
        "location" -> return VLocation
        _ -> fail "could not decode"

data ViberMessage = ViberMessage
    { _mType :: MessageType
    , _mText :: Maybe Text
    }
instance FromJSON ViberMessage where
    parseJSON = withObject "object" $ \o ->
        ViberMessage <$> o .: "type" <*> o .:? "text"

data ViberEvent = EMessage | ESubscribed | EUnsubscribed | EDelivered | ESeen | EFailed | EConversationStarted | EWebhook
instance ToJSON ViberEvent where
    toJSON EMessage = "message"
    toJSON ESubscribed = "subscribed"
    toJSON EUnsubscribed = "unsubscribed"
    toJSON EDelivered = "delivered"
    toJSON ESeen = "seen"
    toJSON EFailed = "failed"
    toJSON EConversationStarted = "conversation_started"
    toJSON EWebhook = "webhook"
instance FromJSON ViberEvent where
    parseJSON  = withText "event" $ \case
        "message" -> return EMessage
        "subscribed" -> return ESubscribed
        "unsubscribed" -> return EUnsubscribed
        "delivered" -> return EDelivered
        "seen" -> return ESeen
        "failed" -> return EFailed
        "conversation_started" -> return EConversationStarted
        "webhook" -> return EWebhook
        _ -> fail "could not decode"

data ViberCallback = ViberCallback
    { _cbEvent :: ViberEvent
    , _cbTimestamp :: Integer
    , _cbMessageToken :: Integer
    , _cbSender :: Maybe MessageSender
    , _cbMessage :: Maybe ViberMessage
    }
instance FromJSON ViberCallback where
    parseJSON = withObject "object" $ \o -> do
        e <- o .: "event"
        ts <- o .: "timestamp"
        mt <- o .: "message_token"
        ms <- o .:? "sender"
        m <- o .:? "message"
        return $ ViberCallback e ts mt ms m

webHookAPI :: Proxy WebHookAPI
webHookAPI = Proxy

webhook :: MessageHandler -> (Text, ViberToken) -> RequestHandler
webhook mh (sn, t) = serve webHookAPI (\_ _ -> webhook')
    where webhook' (ViberCallback EMessage _ mt (Just (MessageSender msid)) (Just (ViberMessage VText (Just txt)))) = do
              liftIO $ handleMessage msid mt txt
              return "ok"
          webhook' (ViberCallback EMessage _ _ _ _) = throwError err400
          webhook' (ViberCallback EWebhook _ _ _ _ ) = return "ok"
          webhook' (ViberCallback ESubscribed _ _ _ _ ) = return "ok"
          webhook' (ViberCallback EUnsubscribed _ _ _ _ ) = return "ok"
          webhook' _ = throwError err404
          handleMessage msid mt txt = do
              resp <- mh (txt, vbUserID msid, vbMessageID mt)
              case resp of
                Nothing -> return ()
                Just resp' -> do
                  m <- newManager tlsManagerSettings
                  res <- runClientM (sendMessage t (messageRequest msid sn resp')) (mkClientEnv m viberBaseUrl)
                  case res of
                    Left err -> exceptionM component "Received error when sending message: " err
                    Right (SendMessageResponse _ s sm)
                      | s /= 0 -> errorL component ["Received error when sending message: ", sm]
                    _ -> return ()

minApiVersion :: Int
minApiVersion = 3

messageRequest :: Text -> Text -> Message -> SendMessageRequest
messageRequest msid sender msg = SendMessageRequest msid (Sender sender) (msgBody msg) minApiVersion (makeKeyboard msg)

makeKeyboard :: Message -> Maybe ViberKeyboard
makeKeyboard msg = ViberKeyboard Nothing . fmap (\t -> ViberButton t t) <$> msgKeyboard msg

vbUserID :: Text -> UserID
vbUserID u = UserID $ sformat ("vb:" % stext) u

vbMessageID :: Integer -> MessageID
vbMessageID u = MessageID $ sformat ("vb:" % int) u

viberGateway :: MessageHandler -> (Text, Text) -> Gateway
viberGateway mh t = Gateway "vb" Nothing (Map.fromList [(webhookPath, webhook mh vt)]) (Just . bindWebhook . snd $ vt)
    where vt = fmap ViberToken t
