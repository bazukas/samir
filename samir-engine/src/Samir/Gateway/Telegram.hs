{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeOperators #-}
module Samir.Gateway.Telegram
( TgAPI(..)
, telegramGateway
) where

import Control.Monad (void)
import Control.Monad.IO.Class (liftIO)
import Data.Monoid ((<>))
import Data.Text (Text, pack)
import Formatting ((%), sformat, int)
import Network.HTTP.Client (newManager, managerResponseTimeout, responseTimeoutMicro)
import Network.HTTP.Client.TLS (tlsManagerSettings)
import Safe (maximumMay)
import Servant
import Web.Telegram.API.Bot hiding (Message)
import qualified Data.Map.Strict as Map

import Samir.Engine.Server (RequestHandler)
import Samir.Gateway.Types
import Samir.Logger
import Samir.Message

pollTimeout :: Int
pollTimeout = 300

component :: String
component = "Samir.Gateway.Telegram"

messageRequest :: Int -> Message -> SendMessageRequest
messageRequest chat_id' msg = SendMessageRequest (ChatId (fromIntegral chat_id')) txt Nothing Nothing Nothing Nothing kbd
    where txt = msgBody msg
          kbd = case msgKeyboard msg of
                  Nothing -> Just (ReplyKeyboardRemove True Nothing)
                  Just bs -> Just (ReplyKeyboardMarkup (fmap toButton bs) (Just True) (Just True) Nothing)
          toButton b = [KeyboardButton b Nothing Nothing]

telegramUserID :: Int -> UserID
telegramUserID c = UserID $ sformat ("tg:" % int) c

telegramMessageID :: Int -> MessageID
telegramMessageID c = MessageID $ sformat ("tg:" % int) c

-- LongPoll API

messageData :: Update -> Maybe (Text, Int, Int)
messageData update = do
    msg <- message update
    txt <- text msg
    user <- from msg
    return (txt, user_id user, message_id msg)

processUpdate :: MessageHandler -> Update -> TelegramClient Int
processUpdate msgHandler update = do
    case messageData update of
        Nothing -> return ()
        Just (txt, chatId, messageId) -> do
            resp <- liftIO $ msgHandler (txt, telegramUserID chatId, telegramMessageID messageId)
            case resp of
              Nothing -> return ()
              Just resp' -> void $ sendMessageM (messageRequest chatId resp')
    return $ update_id update

updatesLoop :: Int -> MessageHandler -> TelegramClient ()
updatesLoop offset msgHandler = do
    response <- getUpdatesM (GetUpdatesRequest (Just offset) Nothing (Just pollTimeout) (Just ["message"]))
    let updates = result response
    update_ids <- mapM (processUpdate msgHandler) updates
    updatesLoop (maybe offset (+1) (maximumMay update_ids)) msgHandler

telegramStart :: Token -> MessageHandler -> IO ()
telegramStart token msgHandler = do
    manager <- newManager (tlsManagerSettings { managerResponseTimeout =
                                                    responseTimeoutMicro ((pollTimeout + 10) * 10^(6 :: Int)) })
    resE <- runTelegramClient token manager (updatesLoop 0 msgHandler)
    case resE of
      Left e -> errorL component ["Encountered error: ", pack . show $ e]
      Right _ -> warningM component "Updates loop exited"

-- Webhook API

webhookPath :: Text
webhookPath = "webhook"

type WebHookAPI' = ReqBody '[JSON] Update :> Post '[PlainText] Text
type WebHookAPI = Capture "project_name" Text :> Capture "gw_name" Text :> "webhook" :> WebHookAPI'

webHookAPI :: Proxy WebHookAPI
webHookAPI = Proxy

webhook :: MessageHandler -> Token -> RequestHandler
webhook mh t = serve webHookAPI (\_ _ -> webhook')
    where webhook' upd = do
            m <- liftIO $ newManager tlsManagerSettings
            res <- liftIO $ runTelegramClient t m (processUpdate mh upd)
            case res of
              Left err -> liftIO $ exceptionM component "Received error when processing udpate: " err
              Right _ -> return ()
            return "ok"

bindWebhook :: Token -> Text -> IO ()
bindWebhook t host = do
    debugM component "Setting Telegram webhook"
    m <- newManager tlsManagerSettings
    res <- runTelegramClient t m (setWebhookM $ setWebhookRequest' url)
    case res of
      Left err -> exceptionM component "Received error when setting webhook: " err
      Right (Response False _) -> errorM component "Could not set webhook!"
      Right (Response True _) -> return ()
    where url = host <> "/" <> webhookPath

data TgAPI = TgLongPoll | TgCallback

telegramGateway :: MessageHandler -> (TgAPI, Text) -> Gateway
telegramGateway mh (TgLongPoll, token) = Gateway "tg" (Just $ telegramStart (Token token) mh) Map.empty Nothing
telegramGateway mh (TgCallback, token) = Gateway "tg" Nothing (Map.fromList [(webhookPath, webhook mh (Token token))])
                                                              (Just $ bindWebhook (Token token))
